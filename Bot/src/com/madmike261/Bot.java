package com.madmike261;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.currency.Currency;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madmike261.bots.FlipperBot;
import com.madmike261.customclasses.BotInstance;
import com.madmike261.customclasses.BotSettings;
import com.madmike261.customclasses.TradePair;
import com.madmike261.runstate.RunState;
import com.madmike261.util.CustomLogger;
import com.madmike261.util.JSON;
import com.madmike261.util.TimeUtil;

public class Bot {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	private static RunState runState = new RunState();
	private static BotSettings botSettings = new BotSettings();
	private static boolean logState = false;
	
	// TODO check error catching of order placement
	
	// TODO intent log
	// TODO combine sells to avoid small orders, on error store to disk? - getting
	// rekt on low volume coins like OMG
	// always store sells to disk, combine and post when above minimum amount
	
	// TODO review slot management to add more slots when at boundary (if returning less than requested slots, add more)
	
	// TODO add amount sold to slots, sell atleast 99% before opening the slot for buys
	
	// TODO only post buy orders down to minimum balance
	
	// TODO move offsetPercentage, profitPercentage, takeProfitPercentage to per coin
	// TODO add spacing detection for slot recalculation
	
	// TODO add indicator to buy and sell orders to pair them to a coin config -> there is an order field for this on bfx.
	// TODO do not run if processed buys can't be opened
	
	// TODO intent log -> add all buys and sells to it and process at the end, check next run for success. Add custom ID to buys and sells for identification of bot
	
	// TODO always request all wanted slot and verify them, close all other buy orders
	// TODO always validate orders to close, because they are too far from price
	
	// TODO better calculation of take profit -> currently taking too much coin -> calc cost and profits, take x% of profit and calc how many coins that is, reduce those coins from original
	
	// TODO support 40% ish pullbacks

	// TODO input range with tradepair params and calculate required number of
	// orders/usd for full coverage
	// TODO check placement mechanics for post only and further ahead of price, done
	// for buy,
	// also do for sells (only if with X% of price), on fail retry and add fees on
	// top so we dont mind if it market sells)

	// TODO add function to reset buy slots to 0 / check open and stored sell orders
	// for slot matches - check if slots count matches orders

	// TODO properly evaluate canceling of sell orders and storing them to disk,
	// what happens if trying to cancel, but the order got filled...
	// TODO update specific data for every execution of flipperbot.run
	// TODO add more know/correct error codes that do not need retries -> for
	// example insufficient funds
	// TODO add handling of insufficient funds errors or others that need to be
	// correctly handled by bot logic
	
	// TODO calculate total bought + sold = profit in coins and money all-time + last month

	public static void main(String[] args) {
		Bot.init();
		CustomLogger.init(runState.getRunningDir(), Level.INFO);

		for(BotInstance currentInstance : botSettings.getBotSettings()) {
			runState = new RunState();
			runState.init(currentInstance);
			LOG.log(Level.INFO, "Current Bot: " + runState.getBotName());
			LOG.log(Level.INFO, "Current available USD Balance: " + runState.getWallet().getBalance(Currency.getInstance("USD")).getAvailable());

			for(TradePair currentPair : currentInstance.getTradePairs()) {
				FlipperBot.run(runState, currentPair);

				if(logState) {
					Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
					String json = gson.toJson(currentPair);
					LOG.log(Level.CONFIG, json);

					gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
					json = gson.toJson(runState);
					LOG.log(Level.CONFIG, json);
					TimeUtil.sleep(3000);
				}
			}
			
			LOG.log(Level.INFO, "Sleeping before next bot!");
			TimeUtil.sleep(3000);
		}
	}

	private static void init() {
		String botSettingsFile = runState.getRunningDir() + File.separator + "BotSettings.json";
		botSettings = JSON.getBotSettings(botSettingsFile);
	}
}