package com.madmike261.bots;

import java.io.File;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.UserTrades;

import com.madmike261.DataPost;
import com.madmike261.customclasses.FlipperBotSettings;
import com.madmike261.customclasses.PriceSlot;
import com.madmike261.customclasses.PriceSlots;
import com.madmike261.customclasses.TradePair;
import com.madmike261.ordermanagement.ListManagement;
import com.madmike261.ordermanagement.OrderProcessing;
import com.madmike261.runstate.RunState;
import com.madmike261.util.JSON;
import com.madmike261.util.SlotManagement;
import com.madmike261.util.TimeUtil;

public class FlipperBot {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");

	public static void run(RunState runState, TradePair tradePair) {
		boolean run = init(runState, tradePair);
		
		//process closed sell orders to update slots, store for logging
		validateClosedSellOrders(runState);

		//process closed buy orders to update slots, place sells, store for logging
		validateClosedBuyOrders(runState, tradePair);
		
		//process open sell orders, limit to 3,5% above price, store to disk if needed


		//process stored sell orders, limit to 2,5% above price
		
		if(run){
			//process open buy orders / place new buy orders
			validateOpenBuyOrders(runState, tradePair);
		}
		
		//cancel open buy orders
		cancelOpenBuyOrders(runState, tradePair);
		TimeUtil.sleep(1000);
	}
	
	private static boolean init(RunState runState, TradePair instanceSettings) {
		boolean returnBoolean = false;
		runState.update(instanceSettings);
		LOG.log(Level.INFO, runState.getTickerPlainName() + ", current Price: " + runState.getTicker().getLast());

		File directory = new File(String.valueOf(runState.getRunningDir() + runState.getBotName()));
		if(!directory.exists()){
			LOG.log(Level.INFO, "Creating Bot directory: " + directory.toString());
			directory.mkdir();
		}

		if(instanceSettings.getStartTradingBalance().compareTo(runState.getWallet().getBalance(Currency.getInstance(instanceSettings.getBaseCurrency())).getAvailable()) == -1) {
			returnBoolean = true;
		}
		else {
			LOG.log(Level.INFO, "Not enough balance to run on: " + instanceSettings.getTradePair() + ", Balance required: " + instanceSettings.getStartTradingBalance());
		}
		
		if(runState.getTicker().getLast().compareTo(instanceSettings.getUpperBoundary()) == 1) {
			LOG.log(Level.INFO, "Price above upperBoundary: " + instanceSettings.getUpperBoundary());
			returnBoolean = false;
		}
		
		if(runState.getTicker().getLast().compareTo(instanceSettings.getLowerBoundary()) == -1) {
			LOG.log(Level.INFO, "Price below lowerBoundary: " + instanceSettings.getLowerBoundary());
			returnBoolean = false;
		}

		return returnBoolean;
	}

	private static void validateClosedSellOrders(RunState runState) {
		if(runState.getClosedSellOrders().getUserTrades().isEmpty()) {
			return;
		}
		
		String tradesFile = runState.getRunningDir() + runState.getBotName() + File.separator + runState.getTickerPlainName() + "_ProcessedClosedSellOrders.json";
		UserTrades storedOrders = JSON.getProcessedClosedOrders(tradesFile);
		UserTrades currentOrders = runState.getClosedSellOrders();
		
		//store latest closed sell orders to disk
		if(!(storedOrders.getUserTrades().equals(currentOrders.getUserTrades()))) {
			LOG.log(Level.FINE, "Changes is closedSellOrders, storing new ClosedOrdersFile.");
			JSON.createProcessedClosedOrdersFile(tradesFile, currentOrders);
		}
		else {
			return;
		}

		//remove already processed buys from the list
		UserTrades ordersToProcess = ListManagement.removeProcessedOrders(currentOrders, storedOrders);

		//process closed sell order
		OrderProcessing.processClosedSells(ordersToProcess, runState);
	}

	private static void validateClosedBuyOrders(RunState runState, TradePair tradePair) {
		if(runState.getClosedBuyOrders().getUserTrades().isEmpty()) {
			return;
		}
		
		String tradesFile = runState.getRunningDir() + runState.getBotName() + File.separator + runState.getTickerPlainName() + "_ProcessedClosedBuyOrders.json";
		UserTrades storedOrders = JSON.getProcessedClosedOrders(tradesFile);
		UserTrades currentOrders = runState.getClosedBuyOrders();
		
		//store latest closed buy orders to disk
		if(!(storedOrders.getUserTrades().equals(currentOrders.getUserTrades()))) {
			LOG.log(Level.FINE, "Changes is closedBuyOrders, storing new ClosedOrdersFile.");
			JSON.createProcessedClosedOrdersFile(tradesFile, currentOrders);
		}
		else {
			return;
		}
		

		//remove already processed buys from the list
		UserTrades ordersToProcess = ListManagement.removeProcessedOrders(currentOrders, storedOrders);
		
		//process unprocessed buys
		if(!ordersToProcess.getUserTrades().isEmpty()) {
			LOG.log(Level.FINE, "processing unprocessed buys: " + ordersToProcess.getUserTrades().size());
			
			//handle partial trades
			UserTrades editedOrdersToProcess = ListManagement.processPartialBuys(ordersToProcess, tradePair);
			
			//process filtered closed buys
			OrderProcessing.processClosedBuys(editedOrdersToProcess, runState, tradePair);
		}
	}

	private static void validateOpenBuyOrders(RunState runState, TradePair tradePair) {
		PriceSlots potentialSlots = SlotManagement.getSlots(runState, tradePair);

		//remove slots that match currently open buy orders
		if((potentialSlots.getPriceSlots().size() > 0) && (runState.getOpenBuyOrders().getOpenOrders().size() > 0)) {
			PriceSlots tempSlots = new PriceSlots();
			for(PriceSlot currentSlot : potentialSlots.getPriceSlots()) {
				boolean found = false;

				for(LimitOrder currentOrder : runState.getOpenBuyOrders().getOpenOrders()) {
					if(currentSlot.getPrice().setScale(currentOrder.getLimitPrice().scale(),BigDecimal.ROUND_HALF_EVEN).stripTrailingZeros().compareTo(currentOrder.getLimitPrice().stripTrailingZeros()) == 0) {
						found = true;
					}
				}

				if(found) {
					tempSlots.getPriceSlots().add(currentSlot);
				}
			}

			for(PriceSlot tempSlot : tempSlots.getPriceSlots()) {
				potentialSlots.getPriceSlots().remove(tempSlot);
			}
		}

		if(potentialSlots.getPriceSlots().size() > 0) {
			for(PriceSlot currentSlot : potentialSlots.getPriceSlots()) {
				if(currentSlot.getSlotCount() < tradePair.getMaxSlotCount()) {
					DataPost.placeNewLimitBuyOrder(runState, tradePair, currentSlot.getPrice());
				}
			}
		}
	}
	
	private static void cancelOpenBuyOrders(RunState runState, TradePair tradePair) {
		// generate new cancel orders
		if (!runState.getOpenBuyOrders().getOpenOrders().isEmpty()) {
			PriceSlots allowedSlots = SlotManagement.getSlots(runState, tradePair);
			
			for(LimitOrder currentOrder : runState.getOpenBuyOrders().getOpenOrders()) {
				LOG.log(Level.FINE, "CurrentOrder: " + currentOrder.getLimitPrice());
				boolean found = false;
				for(PriceSlot currentSlot : allowedSlots.getPriceSlots()) {
					LOG.log(Level.FINE, "CurrentSlot: " + currentSlot.getPrice());
					if(currentOrder.getLimitPrice().stripTrailingZeros().equals(currentSlot.getPrice().stripTrailingZeros())) {
						LOG.log(Level.FINE, "Matched open order to allowed price slot.");
						found = true;
					}
					if(currentOrder.getLimitPrice().stripTrailingZeros().equals(runState.getTicker().getLast().stripTrailingZeros())) {
						LOG.log(Level.FINE, "Matched to current prize, allowing.");
						found = true;
					}
				}
				if(!found) {
					LOG.log(Level.INFO, "Cancelling order: " + currentOrder.toString());
					DataPost.cancelOpenOrder(runState, currentOrder.getId());
				}
			}
		}
		
		/* old cancel orders
		if (runState.getOpenBuyOrders().getOpenOrders().size() > tradePair.getMaxOpenBuyOrders()) {
			int ordersToClose = runState.getOpenBuyOrders().getOpenOrders().size() - tradePair.getMaxOpenBuyOrders();
			LOG.log(Level.INFO, "Starting cancel, listsize: " + runState.getOpenBuyOrders().getOpenOrders().size() + ", orders to close: " + ordersToClose);

			for (int i = 1; i <= ordersToClose; i++) {
				DataPost.cancelOpenOrder(runState, runState.getOpenBuyOrders().getOpenOrders().get((i - 1)).getId());
			}
		}
		*/
	}

	@SuppressWarnings("unused")
	private static void validateOpenSellOrders(RunState runState, FlipperBotSettings botSettings) {
		//needs work
	}
}